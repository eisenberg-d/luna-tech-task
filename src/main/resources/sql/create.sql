CREATE TABLE COUNTRY (
  id             INTEGER     NOT NULL,
  code           VARCHAR(2)  NOT NULL,
  name           VARCHAR(50) NOT NULL,
  continent      VARCHAR(2)  NOT NULL,
  wikipedia_link VARCHAR(100),
  keywords       VARCHAR(100),
  PRIMARY KEY (id),
  UNIQUE INDEX (code, name)
);


CREATE TABLE AIRPORT (
  id                INTEGER,
  ident             VARCHAR NOT NULL,
  type              VARCHAR NOT NULL,
  name              VARCHAR NOT NULL,
  latitude_deg      DOUBLE  NOT NULL,
  longitude_deg     DOUBLE  NOT NULL,
  elevation_ft      INTEGER,
  continent         VARCHAR NOT NULL,
  iso_country       VARCHAR NOT NULL,
  iso_region        VARCHAR NOT NULL,
  municipality      VARCHAR,
  scheduled_service VARCHAR NOT NULL,
  gps_code          VARCHAR,
  iata_code         VARCHAR,
  local_code        VARCHAR,
  home_link         VARCHAR,
  wikipedia_link    VARCHAR,
  keywords          VARCHAR,
  PRIMARY KEY (id),
  CONSTRAINT country_fk FOREIGN KEY (iso_country) REFERENCES COUNTRY (code)
);


CREATE TABLE RUNWAY (
  id                        INTEGER NOT NULL,
  airport_ref               VARCHAR NOT NULL,
  airport_ident             VARCHAR NOT NULL,
  length_ft                 INTEGER,
  width_ft                  INTEGER,
  surface                   VARCHAR,
  lighted                   BOOLEAN NOT NULL,
  closed                    BOOLEAN NOT NULL,
  le_ident                  VARCHAR,
  le_latitude_deg           DOUBLE,
  le_longitude_deg          DOUBLE,
  le_elevation_ft           INTEGER,
  le_heading_degT           DOUBLE,
  le_displaced_threshold_ft INTEGER,
  he_ident                  VARCHAR,
  he_latitude_deg           DOUBLE,
  he_longitude_deg          DOUBLE,
  he_elevation_ft           INTEGER,
  he_heading_degT           DOUBLE,
  he_displaced_threshold_ft INTEGER,
  PRIMARY KEY (id),
  CONSTRAINT airport_fk FOREIGN KEY (airport_ref) REFERENCES AIRPORT (id)
);