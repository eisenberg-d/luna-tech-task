INSERT INTO Country SELECT *
                    FROM CSVREAD('classpath:/csv/countries.csv');
INSERT INTO Airport SELECT *
                    FROM CSVREAD('classpath:/csv/airports.csv');
INSERT INTO Runway SELECT *
                   FROM CSVREAD('classpath:/csv/runways.csv');