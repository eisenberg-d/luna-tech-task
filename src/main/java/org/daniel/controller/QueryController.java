package org.daniel.controller;


import org.apache.commons.lang3.tuple.ImmutablePair;
import org.daniel.entity.Airport;
import org.daniel.repository.QueryRepository;
import org.daniel.repository.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Controller that accepts user requests
 */
@RestController
public class QueryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryController.class);

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private QueryRepository queryRepository;


    /**
     * Asks for airports and related runways by specified country code
     *
     * @param countryCode
     * @return list of airport with assigned runways
     */
    @RequestMapping(value = "/airports", method = RequestMethod.GET)
    public List<Airport> getAirportsAndRunways(@RequestParam(value = "countryCode", required = true) String countryCode,
                                               @RequestParam(value = "countryName", required = true) String countryName) {
        LOGGER.info("/airports method was called with countryCode: {} and countryName: {}", countryCode, countryName);
        return queryRepository.getAirportsByCountry(countryCode, countryName);
    }

    /**
     * Method that reports 10 countries with highest number of airports with count
     * and 10 countries with lowest number of airports
     *
     * @return Map with two entries. 1. Top ten and related list and 2. Low ten and related list.
     */
    @RequestMapping(value = "/countries/topAndLowTen", method = RequestMethod.GET)
    public Map<String, List<ImmutablePair<String, Integer>>> topAndLowTen() {
        LOGGER.info("/countries/topAndLowTen method was called");
        return reportRepository.getTopTenCountries();
    }

    /**
     * Method reports types of runways per country
     *
     * @return Map of countries and associated runway types
     */
    @RequestMapping(value = "/runways/types", method = RequestMethod.GET)
    public Map<String, List<String>> runwayTypes() {
        LOGGER.info("/runways/types was called");
        return reportRepository.runwayTypes();
    }

    /**
     * Method reports top 10 most common runway identifications
     *
     * @return List of pairs identification -> count
     */
    @RequestMapping(value = "/runways/identifications", method = RequestMethod.GET)
    public List<ImmutablePair<String, Integer>> tenMostCommonRunway() {
        LOGGER.info("/runways/identifications method was called");
        return reportRepository.topTenRunways();
    }

}
