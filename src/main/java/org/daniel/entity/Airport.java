package org.daniel.entity;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 *
 */
@Data
@Builder
public class Airport {

    private final Long id;
    private final String ident;
    private final String type;
    private final String name;
    private final Double latitudeDeg;
    private final Double longitudeDeg;
    private final Integer elevationFt;
    private final String continent;
    private final String iso_country;
    private final String iso_region;
    private final String municipality;
    private final String scheduledService;
    private final String gpsCode;
    private final String iataCode;
    private final String localCode;
    private final String homeLink;
    private final String wikipediaLink;
    private final String finalkeywords;
    private final List<Runway> runways;

}
