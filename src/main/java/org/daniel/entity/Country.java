package org.daniel.entity;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Entity that describes country
 */
@Data
@Builder
public class Country {

    private final Long id;
    private final String code;
    private final String name;
    private final String continent;
    private final String wikipediaLink;
    private final String keywords;
    private final List<Airport> airports;
}