package org.daniel.entity;

import lombok.Builder;
import lombok.Data;

/**
 * Entity that describes Runway
 */
@Data
@Builder
public class Runway {

    private final Long id;
    private final String airportRef;
    private final String airportIdent;
    private final Integer lengthFt;
    private final Integer widthFt;
    private final String surface;
    private final boolean lighted;
    private final boolean closed;
    private final String leIdent;
    private final Double leLatitudeDeg;
    private final Double leLongitudeDeg;
    private final String leElevationFt;
    private final Double leHeadingDegT;
    private final Integer leDisplacedThresholdFt;
    private final String heIdent;
    private final Double heLatitudeDeg;
    private final Double heLongitudeDeg;
    private final Integer heElevationFt;
    private final Double heHeadingDegT;
    private final Integer heDisplacedThresholdFt;

}
