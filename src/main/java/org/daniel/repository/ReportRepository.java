package org.daniel.repository;

import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.List;
import java.util.Map;

/**
 * Repository for retrieving reports
 */
public interface ReportRepository {


    /**
     * Requests for 10 countries with highest number of airports
     * and countries with lowest number of airports.
     *
     * @return Map of top ten and low ten list of airports
     */
    Map<String, List<ImmutablePair<String, Integer>>> getTopTenCountries();

    /**
     * Print the top 10 most common runway identifications (indicated in "le_ident" column)
     *
     * @return List of pairs runway identification -> count
     */
    List<ImmutablePair<String, Integer>> topTenRunways();


    /**
     * Type of runways (as indicated in "surface" column) per country
     *
     * @return Map of countries and associated runway types
     */
    Map<String, List<String>> runwayTypes();
}
