package org.daniel.repository;

import org.daniel.entity.Airport;
import org.daniel.entity.Runway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class QueryRepositoryImpl implements QueryRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryRepository.class);

    private static final String GET_AIRPORTS_BY_CODE = "SELECT * FROM AIRPORT arp INNER JOIN RUNWAY rnw ON " +
            "rnw.airport_ref=arp.id INNER JOIN COUNTRY ctr ON ctr.code = arp.iso_country " +
            "WHERE ctr.code =:code OR ctr.name like :name";

    private static final AirportExtractor AIRPORT_EXTRACTOR = new AirportExtractor();
    private static final AirportMapper AIRPORT_MAPPER = new AirportMapper();
    private static final RunwayMapper RUNWAY_MAPPER = new RunwayMapper();

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Airport> getAirportsByCountry(String code, String name) {
        LOGGER.debug("db was asked with sql: {} with parameter: {}", GET_AIRPORTS_BY_CODE, code);
        return jdbcTemplate.query(GET_AIRPORTS_BY_CODE, new HashMap<String, String>() {{
            put("code", code);
            put("name", "%"+name+"%");
        }}, AIRPORT_EXTRACTOR);
    }


    private static class AirportExtractor implements ResultSetExtractor<List<Airport>> {

        @Override
        public List<Airport> extractData(ResultSet rs) throws SQLException, DataAccessException {
            Map<Long, Airport> airportMap = new HashMap<Long, Airport>();
            while (rs.next()) {
                Long airportId = rs.getLong("id");
                Airport airport = airportMap.get(airportId);
                if (null == airport) {
                    airport = AIRPORT_MAPPER.mapRow(rs, rs.getRow());
                    airportMap.put(airportId, airport);
                }
                Runway runway = RUNWAY_MAPPER.mapRow(rs, rs.getRow());
                airport.getRunways().add(runway);
            }
            return new ArrayList<>(airportMap.values());
        }
    }

    private static class AirportMapper implements RowMapper<Airport> {

        @Override
        public Airport mapRow(ResultSet resultSet, int i) throws SQLException {
            return Airport.builder().id(resultSet.getLong("id")).continent(resultSet.getString("continent"))
                    .elevationFt(resultSet.getInt("elevation_ft")).finalkeywords(resultSet.getString("keywords"))
                    .gpsCode(resultSet.getString("gps_code")).homeLink(resultSet.getString("home_link"))
                    .iataCode(resultSet.getString("iata_code")).iso_country(resultSet.getString("iso_country"))
                    .ident(resultSet.getString("ident")).iso_region(resultSet.getString("iso_region"))
                    .latitudeDeg(resultSet.getDouble("latitude_deg")).longitudeDeg(resultSet.getDouble("longitude_deg"))
                    .municipality(resultSet.getString("municipality")).type(resultSet.getString("type"))
                    .name(resultSet.getString("name")).scheduledService(resultSet.getString("scheduled_service"))
                    .runways(new ArrayList<>()).build();
        }
    }

    private static class RunwayMapper implements RowMapper<Runway> {

        @Override
        public Runway mapRow(ResultSet rs, int i) throws SQLException {
            return Runway.builder().airportIdent(rs.getString("airport_ident"))
                    .closed(rs.getBoolean("closed")).airportRef(rs.getString("airport_ref"))
                    .heDisplacedThresholdFt(rs.getInt("he_displaced_threshold_ft")).heElevationFt(rs.getInt("he_elevation_ft"))
                    .heHeadingDegT(rs.getDouble("he_heading_degT")).heIdent(rs.getString("he_ident"))
                    .id(rs.getLong("id")).lighted(rs.getBoolean("lighted")).lengthFt(rs.getInt("length_ft"))
                    .surface(rs.getString("surface")).heLatitudeDeg(rs.getDouble("he_latitude_deg"))
                    .heLongitudeDeg(rs.getDouble("he_longitude_deg")).widthFt(rs.getInt("width_ft")).build();
        }
    }

}
