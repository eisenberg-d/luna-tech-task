package org.daniel.repository;

import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ReportRepositoryImpl implements ReportRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportRepository.class);

    private static final String GET_TOP_AND_LOW_TEN = "SELECT ctr.name as name, count(1) as count FROM COUNTRY ctr INNER JOIN AIRPORT " +
            "arp ON ctr.code = arp.iso_country group by ctr.name order by count(1) %s limit 10";

    private static final String RUNWAY_TYPES = "SELECT  DISTINCT ctr.name as country, rnw.surface as runwayType FROM COUNTRY ctr INNER JOIN AIRPORT arp ON ctr.code = arp.iso_country " +
            "INNER JOIN RUNWAY rnw ON rnw.airport_ref=arp.id";

    private static final String TOP_TEN_RUNWAY = "SELECT le_ident as identification, count(1) as count FROM RUNWAY GROUP BY le_ident order by count(1) desc limit 10";

    private static final RunwayTypeExtractor RUNWAY_TYPE_EXTRACTOR = new RunwayTypeExtractor();

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public Map<String, List<ImmutablePair<String, Integer>>> getTopTenCountries() {
        return new HashMap<String, List<ImmutablePair<String, Integer>>>() {{
            put("topTen", getTenByDirection("DESC"));
            put("lowTen", getTenByDirection("ASC"));
        }};
    }

    public Map<String, List<String>> runwayTypes() {
        LOGGER.debug("db was asked with sql: {}", RUNWAY_TYPES);
        return jdbcTemplate.query(RUNWAY_TYPES, new HashMap<>(), RUNWAY_TYPE_EXTRACTOR);
    }


    public List<ImmutablePair<String, Integer>> topTenRunways() {
        return jdbcTemplate.query(TOP_TEN_RUNWAY, new HashMap<>(), new RowMapper<ImmutablePair<String, Integer>>() {
            @Override
            public ImmutablePair<String, Integer> mapRow(ResultSet rs, int rowNum) throws SQLException {
                return ImmutablePair.of(rs.getString("identification"), rs.getInt("count"));
            }
        });
    }


    @VisibleForTesting
    List<ImmutablePair<String, Integer>> getTenByDirection(String direction) {
        LOGGER.debug("db was asked with sql: {} and order direction: {}", GET_TOP_AND_LOW_TEN, direction);
        return jdbcTemplate.query(String.format(GET_TOP_AND_LOW_TEN, direction), new HashMap<>(),
                new RowMapper<ImmutablePair<String, Integer>>() {
                    @Override
                    public ImmutablePair<String, Integer> mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return ImmutablePair.of(rs.getString("name"), rs.getInt("count"));
                    }
                });
    }


    private static class RunwayTypeExtractor implements ResultSetExtractor<Map<String, List<String>>> {

        @Override
        public Map<String, List<String>> extractData(ResultSet rs) throws SQLException, DataAccessException {
            Map<String, List<String>> countryRunways = new HashMap<String, List<String>>();
            while (rs.next()) {
                String country = rs.getString("country");
                List<String> runways = countryRunways.get(country);
                if (null == runways) {
                    runways = new ArrayList<>();
                    countryRunways.put(country, runways);
                }
                runways.add(rs.getString("runwayType"));
            }
            return countryRunways;
        }
    }


}
