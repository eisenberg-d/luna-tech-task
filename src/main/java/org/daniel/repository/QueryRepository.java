package org.daniel.repository;

import org.daniel.entity.Airport;

import java.util.List;

/**
 * Repository for making queries with user defined parameters
 */
public interface QueryRepository {

    /**
     * Query for airports and associated runways for specified country code
     *
     * @param countryCode
     * @param countryName
     * @return list of airports
     */
    List<Airport> getAirportsByCountry(String countryCode, String countryName);
}
