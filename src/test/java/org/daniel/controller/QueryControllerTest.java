package org.daniel.controller;


import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.daniel.entity.Airport;
import org.daniel.repository.QueryRepository;
import org.daniel.repository.ReportRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for {@link QueryController}
 */
@RunWith(MockitoJUnitRunner.class)
public class QueryControllerTest {


    @Mock
    private QueryRepository queryRepository;

    @Mock
    private ReportRepository reportRepository;

    private MockMvc mockMvc;

    @InjectMocks
    private QueryController controller;


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void shouldReturnListOfAirports() throws Exception {
        //prepare
        Airport airport = Airport.builder().iso_country("US").name("Big airport").build();
        when(queryRepository.getAirportsByCountry(anyString(), anyString()))
                .thenReturn(Lists.newArrayList(airport));
        //execute
        mockMvc.perform(get("/airports?countryCode=US&countryName=USA")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string(containsString("Big airport")));

        //verify
        verify(queryRepository).getAirportsByCountry(anyString(), anyString());


    }

    @Test
    public void shouldReturnTopTenAndLowTen() throws Exception {
        //prepare
        Map<String, List<ImmutablePair<String, Integer>>> expectedMap =
                new HashMap<String, List<ImmutablePair<String, Integer>>>() {{
                    put("topTen", Lists.newArrayList(ImmutablePair.of("Brazil", 10)));
                    put("lowTen", Lists.newArrayList(ImmutablePair.of("Canada", 1)));
                }};


        when(reportRepository.getTopTenCountries())
                .thenReturn(expectedMap);
        //execute & verify
        mockMvc.perform(get("/countries/topAndLowTen")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string(containsString("topTen")))
                .andExpect(content().string(containsString("Canada")))
                .andExpect(content().string(containsString("lowTen")))
                .andExpect(content().string(containsString("Brazil")));

        //verify
        verify(reportRepository).getTopTenCountries();

    }

    @Test
    public void shouldReturnRunwayTypes() throws Exception {
        //prepare
        when(reportRepository.runwayTypes())
                .thenReturn(new HashMap<String, List<String>>() {{
                    put("Ukraine", Lists.newArrayList("sand", "asphalt"));
                }});
        //execute
        mockMvc.perform(get("/runways/types")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string(containsString("Ukraine")))
                .andExpect(content().string(containsString("sand")))
                .andExpect(content().string(containsString("asphalt")));

        //verify
        verify(reportRepository).runwayTypes();
    }

    @Test
    public void shouldReturnTopCommonIdentifications() throws Exception {
        //prepare
        when(reportRepository.topTenRunways())
                .thenReturn(Lists.newArrayList(ImmutablePair.of("some_ident", 1), ImmutablePair.of("another_ident", 2)));
        //execute
        mockMvc.perform(get("/runways/identifications")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string(containsString("some_ident")))
                .andExpect(content().string(containsString("another_ident")));

        //verify
        verify(reportRepository).topTenRunways();
    }
}
