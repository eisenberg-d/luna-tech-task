package org.daniel.repository;


import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.daniel.TestDbConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Tests for {@link ReportRepository}
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestDbConfig.class, ReportRepositoryImpl.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DbUnitConfiguration(databaseConnection = {"h2DataSource"})
@Transactional
public class ReportRepositoryTest {

    @Autowired
    private ReportRepositoryImpl repository;

    @Test
    @DatabaseSetup("/testtopten.xml")
    public void testSelectTopTen() {
        //execute
        List<ImmutablePair<String, Integer>> actualResult = repository.getTenByDirection("DESC");
        //verify
        assertNotNull(actualResult);
        assertEquals(actualResult.size(), 2);
        assertTrue(actualResult.get(0).getRight() >= actualResult.get(1).getRight());
    }

    @Test
    @DatabaseSetup("/testtopten.xml")
    public void testSelectLowTen() {
        //execute
        List<ImmutablePair<String, Integer>> actualResult = repository.getTenByDirection("ASC");
        //verify
        assertNotNull(actualResult);
        assertEquals(actualResult.size(), 2);
        assertTrue(actualResult.get(0).getRight() <= actualResult.get(1).getRight());
    }

    @Test
    @DatabaseSetup("/testrunwaytypes.xml")
    public void testSelectRunwayTypes() {
        //execute
        Map<String, List<String>> actualResult = repository.runwayTypes();
        //verify
        assertNotNull(actualResult);
        assertEquals(actualResult.size(), 2);
        actualResult.entrySet().stream().forEach(entry -> {
            assertEquals(entry.getValue().size(), 3);
        });
    }

    @Test
    @DatabaseSetup("/teststoprunwayid.xml")
    public void testSelectRunwayIds() {
        //execute
        List<ImmutablePair<String, Integer>> actualResult = repository.topTenRunways();
        //verify
        assertNotNull(actualResult);
        assertEquals(actualResult.size(), 10);
        assertTrue(actualResult.get(0).getLeft().equals("14L"));
    }


}
