package org.daniel.repository;


import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import org.daniel.TestDbConfig;
import org.daniel.entity.Airport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests for {@link QueryRepository}
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestDbConfig.class, QueryRepositoryImpl.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DbUnitConfiguration(databaseConnection = {"h2DataSource"})
@Transactional
public class QueryRepositoryTest {

    @Autowired
    private QueryRepository queryRepository;

    @Test
    @DatabaseSetup(value = "/testselectbycode.xml")
    public void existedCodeShouldReturnValidList() {
        //execute
        List<Airport> actualResult = queryRepository.getAirportsByCountry("AD","Andorra");
        //verify
        assertNotNull(actualResult);
        assertEquals(1, actualResult.size());
        assertEquals(4, actualResult.get(0).getRunways().size());
    }

    @Test
    public void notExistedCodeAndNameShouldReturnEmptyList() {
        //execute
        List<Airport> actualResult = queryRepository.getAirportsByCountry("AB","NotAndorra");
        //verify
        assertNotNull(actualResult);
        assertEquals(0, actualResult.size());
    }

    @Test
    @DatabaseSetup(value = "/testselectbycode.xml")
    public void notExistedCodeButCorrectNameShouldReturnValidList() {
        //execute
        List<Airport> actualResult = queryRepository.getAirportsByCountry("CC","Andorra");
        //verify
        assertNotNull(actualResult);
        assertEquals(1, actualResult.size());
        assertEquals(4, actualResult.get(0).getRunways().size());
    }

}
